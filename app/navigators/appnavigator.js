import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { createSwitchNavigator, createDrawerNavigator } from 'react-navigation';

import { Login, HomeView, EtaView, SofView } from '../components/';
import drawerImage from '../components/segments/drawerImage';

class Logout extends Component {
  componentWillMount() {
    this.props.navigation.navigate('login');
  }
  render() {
    return null;
  }
}

const DrawNav = createDrawerNavigator(
  {
    home: {
      screen: HomeView,
      navigationOptions: {
        drawerLabel: 'Home Page',
        drawerIcon: ({ tintColor }) => <Icon name="home" size={24} />
      }
    },
    eta: {
      screen: EtaView,
      navigationOptions: {
        drawerLabel: 'Update ETA',
        drawerIcon: ({ tintColor }) => <Icon name="compass" size={24} />
      }
    },
    sof: {
      screen: SofView,
      navigationOptions: {
        drawerLabel: 'Statements of Fact',
        drawerIcon: ({ tintColor }) => <Icon name="book" size={24} />
      }
    },
    logout: {
      screen: Logout,
      navigationOptions: {
        drawerLabel: 'Logout',
        drawerIcon: ({ tintColor }) => <Icon name="sign-out" size={24} />
      }
    }
  },
  {
    contentComponent: drawerImage
  }
);

const MainNav = createSwitchNavigator({
  login: Login,
  main: DrawNav
});

export default MainNav;
