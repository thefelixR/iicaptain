import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { styles } from '../../config/styles';

import { getAPI } from '../../config/api';
import Timeline from 'react-native-timeline-listview';
import TopHeader from '../segments/topHeader';

class EtaView extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };

    this.data = [
      { time: '09:00', title: 'Event 1', description: 'Event 1 Description' },
      { time: '10:45', title: 'Event 2', description: 'Event 2 Description' },
      { time: '12:00', title: 'Event 3', description: 'Event 3 Description' },
      { time: '14:00', title: 'Event 4', description: 'Event 4 Description' },
      { time: '16:30', title: 'Event 5', description: 'Event 5 Description' }
    ];
  }

  componentWillMount() {
  }

  componentDidMount() {
    getAPI(
      'pcb/port_call/urn:mrn:stm:portcdm:port_call:SEGOT:138b91cc-cff0-471c-86e9-2333ceebf1fb'
    ).then(data => {
      this.setState({
        lastUpdated: data.lastUpdated,
        lastUpdatedBy: data.lastUpdatedBy,
        lastUpdatedState: data.lastUpdatedState,
        lastUpdatedTimeType: data.lastUpdatedTimeType
      });
    });
    getAPI(
      'ptp/port_call/urn:mrn:stm:portcdm:port_call:SEGOT:138b91cc-cff0-471c-86e9-2333ceebf1fb/arrival'
    ).then(data => {
      let events = data.events.map(event => ({
        time: event.definitionId,
        title: event.location,
        description: event.startTime
      }));
      this.setState({
        events: events
      });
    });
  }

  render() {
    return (
      <View>
        <TopHeader title={'Update ETA'} navigation={this.props.navigation} />
        <View style={styles.name}>
          <Text>Last {this.state.lastUpdatedTimeType} time:</Text>
          <Text>{this.state.lastUpdatedState}</Text>
          <Text>at {this.state.lastUpdated}</Text>
          <Text>by {this.state.lastUpdatedBy}</Text>
          <Text />
        </View>
      </View>
    );
  }
}

export default EtaView;
