import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';
import { ListItem, List, Divider } from 'react-native-elements';

import { styles } from '../../config/styles';
import TopHeader from '../segments/topHeader';
import { getAPI } from '../../config/api';

class SofView extends Component {
  render() {
    return (
      <View>
        <TopHeader
          title={'Statements of Fact'}
          navigation={this.props.navigation}
        />
        <ScrollView>
          <List>
            <ListItem
              title={'Arrival at Berth'}
              subtitle={'ACTUAL : 08:00'}
              hideChevron
            />
            <ListItem
              title={'Arrival at Berth'}
              subtitle={'ACTUAL : 08:00'}
              hideChevron
            />
            <ListItem
              title={'Arrival at Berth'}
              subtitle={'ACTUAL : 08:00'}
              hideChevron
            />
            <ListItem
              title={'Arrival at Berth'}
              subtitle={'ACTUAL : 08:00'}
              hideChevron
            />
            <ListItem
              title={'Arrival at Berth'}
              subtitle={'ACTUAL : 08:00'}
              hideChevron
            />
          </List>
        </ScrollView>
      </View>
    );
  }

  getListItems(data) {
    let { events } = data;
    let arr = [];
    for (var i in events) {
      arr.push(
        <ListItem
          title={events[i].definitionId}
          subtitle={events[i].startTimeType}
        />
      );
    }
    return arr;
  }
}

export default SofView;
