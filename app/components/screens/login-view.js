import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  Image,
  TouchableOpacity,
  Animated,
  Keyboard
} from 'react-native';
import hatPic from '../../assets/hat.jpg';
import { getAPI, getAPI2 } from '../../config/api';
import { styles } from '../../config/styles';

const IMAGE_SIZE = 250;
const IMAGE_SIZE_SMALL = 0;

class Login extends Component {
  constructor(props) {
    super(props);

    this.imageHeight = new Animated.Value(IMAGE_SIZE);
    this.inputHeight = new Animated.Value(styles.input.height);
  }

  componentWillMount() {
    this.keyboardDidShowSub = Keyboard.addListener(
      "keyboardDidShow",
      this.keyboardDidShow
    );
    this.keyboardDidHideSub = Keyboard.addListener(
      "keyboardDidHide",
      this.keyboardDidHide
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowSub.remove();
    this.keyboardDidHideSub.remove();
  }

  keyboardDidShow = event => {
    Animated.timing(this.imageHeight, {
      duration: 200,
      toValue: IMAGE_SIZE_SMALL
    }).start();
  };

  keyboardDidHide = event => {
    Animated.timing(this.imageHeight, {
      duration: 200,
      toValue: IMAGE_SIZE
    }).start();
  };

  login() {
    this.props.navigation.navigate("main");
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "flex-start",
          alignItems: "center",
          backgroundColor: "white",
          top: "10%"
        }}
      >
        <Text style={styles.text}>iiCaptain</Text>
        <TouchableOpacity>
          <Animated.Image
            style={{
              height: this.imageHeight
            }}
            resizeMode="contain"
            source={hatPic}
          />
        </TouchableOpacity>
        <TextInput style={styles.input} placeholder={"Username"} />
        <TextInput
          style={styles.input}
          placeholder={"Password"}
          secureTextEntry
        />
        <Button onPress={() => this.login()} title="LOGIN" />
      </View>
    );
  }
}

export default Login;
