import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  TouchableOpacity,
  Icon
} from 'react-native';
import { styles } from '../../config/styles';
import { Header } from 'react-native-elements';
import TopHeader from '../segments/topHeader';

class HomeView extends Component {
  render() {
    return (
      <View>
        <TopHeader title={'Captain Start'} navigation={this.props.navigation} />
        <View style={styles.top}>
          <Text style={styles.title}>iiCaptain</Text>
        </View>

        {/* Home menu grid */}
        <View style={styles.gridRow}>
          <Tile
            text="TOP LEFT"
            onPress={() => {
              // DO NAVIGATION
            }}
          />
          <Tile
            text="TOP RIGHT"
            onPress={() => {
              // DO NAVIGATION
            }}
          />
        </View>
        <View style={styles.gridRow}>
          <Tile
            text="BOTTOM LEFT"
            onPress={() => {
              // DO NAVIGATION
            }}
          />
          <Tile
            text="BOTTOM RIGHT"
            onPress={() => {
              // DO NAVIGATION
            }}
          />
        </View>
      </View>
    );
  }
}

class Tile extends Component {
  render() {
    return (
      <TouchableOpacity
        style={styles.square}
        activeOpacity={0.6}
        onPress={this.props.onPress}
      >
        <Text style={styles.appText}>{this.props.text}</Text>
      </TouchableOpacity>
    );
  }
}

export default HomeView;
