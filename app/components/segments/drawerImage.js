import React, { Component } from 'react';
import { View, ImageBackground, Text } from 'react-native';
import { DrawerItems } from 'react-navigation';

const drawerImage = props => (
  <View>
    <View
      style={{
        backgroundColor: 'white',
        height: 180,
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <ImageBackground
        style={{ height: 200, width: 300 }}
        resizeMode={'contain'}
        source={require('../../assets/boat.jpg')}
      >
        <Text
          style={{
            alignSelf: 'center',
            justifyContent: 'center',
            marginTop: 150,
            fontSize: 25,
            fontFamily: 'monospace',
            color: 'white'
          }}
        >
          RISE Viktoria
        </Text>
      </ImageBackground>
    </View>
    <DrawerItems {...props} />
  </View>
);

export default drawerImage;
