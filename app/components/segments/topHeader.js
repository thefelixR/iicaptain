import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Header } from 'react-native-elements';

import { styles } from '../../config/styles';

const topHeader = props => (
  <Header
    outerContainerStyles={{
      backgroundColor: 'lightgray',
      height: 80,
      borderBottomColor: 'black',
      borderBottomWidth: StyleSheet.hairlineWidth
    }}
    style={styles.header}
    leftComponent={{
      icon: 'menu',
      color: 'black',
      onPress: () => (!!props.navigation ? props.navigation.openDrawer() : null)
    }}
    centerComponent={{
      text: !!props.title ? props.title : 'Title',
      style: {
        fontSize: 20,
        color: 'black',
        //fontFamily: 'monospace',
        fontWeight: 'bold'
      }
    }}
  />
);

export default topHeader;
