import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  header: {
    flex: 1,
    marginVertical: 50,
    backgroundColor: 'white'
  },
  name: {
    alignItems: 'center',
    marginVertical: 20,
    paddingTop: 50
  },
  text: {
    fontSize: 50,
    fontFamily: 'monospace',
    fontWeight: 'bold',
    marginVertical: 20
  },
  appText: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    color: 'white'
  },
  input: {
    width: 70 + '%',
    height: 50,
    backgroundColor: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  button: {
    marginVertical: 50
  },
  top: {
    alignItems: 'center',
    paddingTop: 20,
    height: 150,
    backgroundColor: 'white',
    borderBottomColor: 'black',
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  title: {
    fontSize: 40,
    fontWeight: 'bold',
    fontFamily: 'monospace'
  },
  gridRow: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  square: {
    width: 120,
    height: 120,
    backgroundColor: 'skyblue',
    borderRadius: 10,
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
