import cred from '../../cred.json';

let url = cred.url;

myHeaders = {
    "Content-Type": cred.contentType,
    "X-Custom-Header": cred.XCustomHeader,
    "X-PortCDM-UserId": cred.XPortCDMUserId,
    "X-PortCDM-Password": cred.XPortCDMPassword,
    'X-PortCDM-APIKey': cred.XPortCDMAPIKey,
};

export async function getAPI2(args) {
    try {
        const fetchResult = fetch(
            new Request(url + args, { method: 'GET', headers: myHeaders })
        );
        const response = await fetchResult;
        const jsonData = await response.json();
        return jsonData;
    } catch(e) {
        throw Error(e);
    }
}

export const getAPI = (args) => {
    return fetch(url + args, {
        method: 'GET',
        headers: myHeaders
    }).then((res) => {
        return res.json();
    });
};

export const postAPI = (args, params) => {
    fetch(url + args, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(params),
    })
};