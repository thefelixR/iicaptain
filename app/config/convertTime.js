export const isoToTime = iso => {
    let time = new Date(iso);
    let hours = time.getHours();
    hours = ("0" + hours).slice(-2);
    let minutes = time.getMinutes();
    minutes = ("0" + minutes).slice(-2);
    return hours + ":" + minutes;
};