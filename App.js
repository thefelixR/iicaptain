import React from "react";
import Captain from "./app/Captain";

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      welcomeText: "Welcome to the iiCaptain application!"
    };
  }
  updateText = () => {
    console.log(getAPI("port_call?limit=20"));
    this.setState({ welcomeText: getAPI("port_call?limit=20") });
  };

  render() {
    return <Captain />;
  }
}
